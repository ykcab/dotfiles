ZSH=$HOME/.zshrc
dot=$HOME/.config/dotfiles

# setup GO ENV

#echo "#----------------------- GO ENV ------------------#" >> $ZSH
#echo "export PATH=$PATH:/usr/local/go/bin" >> $ZSH
#echo "export GOPAH=$HOME/go" >> $ZSH
#echo "#------------------------END OF GO ENV------------#" >> $ZSH

# Some aliases
#echo " " >> $ZSH
#echo "# some aliases" >> $ZSH
#echo "alias rl='source $ZSH'" >> $ZSH
#echo "alias gm='git add -A && git commit -a "$1" && git push'" >> $ZSH
#echo "#--------------END-------------------#" >> $ZSH

#source $ZSH

# create symlink
cp $ZSH $dot/zshrc && mv $ZSH .zshrc.original
ln -s $dot/zshrc .zshrc

#rl

# install spacevim
#curl -sLf https://spacevim.org/install.sh | bash

